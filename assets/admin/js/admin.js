jQuery( function( $ ) {
	var wp = window.wp;
	/**
	 * Variations Price Matrix actions
	 */
	var nbt_solutions_admin = {

		/**
		 * Initialize variations actions
		 */
		init: function() {
			$('.nbt-colorpicker' ).wpColorPicker();
		}
	}
	
	nbt_solutions_admin.init();

});