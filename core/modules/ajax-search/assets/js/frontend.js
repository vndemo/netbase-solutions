jQuery( function( $ ) {


	var nbt_ajax_search_load = {
		/**
		 * Init jQuery.BlockUI
		 */
		block: function($el) {
			$el.block({
				message: null,
				overlayCSS: {
					background: '#fff',
					opacity: 0.6
				}
			});
		},

		/**
		 * Remove jQuery.BlockUI
		 */
		unblock: function($el) {
			$el.unblock();
		}
	}
	var nbt_ajax_search = {

		/**
		 * Initialize variations actions
		 */
		init: function() {
        	$(document).on('click', '.nbt-icon-search', this.open_ajax_search);

        	$(window).load(function(){
				//setup before functions
				var typingTimer;                //timer identifier
				var doneTypingInterval = 1000;  //time in ms, 5 second for example
				var $input = $('.nbt-input-search');

				//on keyup, start the countdown
				$input.on('keyup', function () {
					$('.nbt-icon-loading').show();
					$('.nbt-search-results').hide();
					clearTimeout(typingTimer);
					typingTimer = setTimeout(doneTyping, doneTypingInterval);
				});

				//on keydown, clear the countdown 
				$input.on('keydown', function () {
					clearTimeout(typingTimer);
				});

				//user is "finished typing," do something
				function doneTyping () {
					nbt_ajax_search.search_now(typingTimer);
				}
        	});
 

			$(document).mouseup(function(e) 
			{
				if($(".nbt-icon-plugins.open").length){
				    var container = $(".nbt-icon-plugins");
				    var icon = $(".nbt-icon-search");

				    if (!container.is(e.target) && container.has(e.target).length === 0 && !icon.is(e.target) && icon.has(e.target).length === 0) 
				    {
						$('.nbt-search-wrapper').hide();
						container.removeClass('open');
				    }

				    if(icon.is(e.target)){

				    }
				}

			});


		},
		search_now: function(typingTimer){
			$('.nbt-icon-loading').show();
			$('.nbt-search-results').html('');
			$.ajax({
				url: wc_add_to_cart_params.ajax_url,
				data: {
					action:     'nbt_search_now',
					search: $('.nbt-input-search').val()
				},
				type: 'POST',
				datatype: 'json',
				success: function( response ) {
					$('.nbt-icon-plugins').addClass('open');
					var rs = JSON.parse(response);
					if(rs.complete != undefined){
						$('.nbt-search-results').append(rs.result);
						$(".nbt-rs-lists").mCustomScrollbar({
						    theme:"dark"
						});
					}else{
						
					}
					$('.nbt-search-results').show();

					$('.nbt-icon-loading').hide();
					clearTimeout(typingTimer);

				},
				error:function(){
					clearTimeout(typingTimer);
					alert('There was an error when processing data, please try again !');
					$('.nbt-icon-loading').hide();
				}
			});
		},
		open_ajax_search: function(){
			$('.nbt-search-results').html('');
			$('.nbt-input-search').val('');
			var $wrapper = $(this).closest('.nbt-icon-plugins').find('.nbt-search-wrapper');
			if($(this).hasClass('open')){
				$wrapper.hide();
				$(this).removeClass('open');
			}else{
				$wrapper.show();
				$(this).addClass('open');
			}

			return false;
		},
		add_to_cart: function(){
			nbt_ajax_cart_load.block($('body'));
			$.ajax({
				url: wc_add_to_cart_params.ajax_url,
				data: {
					action:     'nbt_add_to_cart',
					product_id: $(this).attr('data-product_id')
				},
				type: 'POST',
				datatype: 'json',
				success: function( response ) {
					
					var rs = JSON.parse(response);
					if(rs.complete != undefined){
						$.growl.notice({message: rs.msg, duration: '2500' });
						$('.cart_list').scrollbar();
					}else{
						window.location.href = rs.url;
					}

					nbt_ajax_cart_load.unblock($('body'));

				},
				error:function(){
					alert('There was an error when processing data, please try again !');
					nbt_ajax_cart_load.unblock($('body'));
				}
			});
			return false;
		},
	}


	nbt_ajax_search.init();
	
});


