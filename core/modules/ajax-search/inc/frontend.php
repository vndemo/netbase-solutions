<?php
class NBT_WooCommerce_AjaxSearch_Frontend{
	protected $args;
	
	function __construct() {
		
		add_action('wp_enqueue_scripts', array($this, 'embed_style'));

		add_action( 'wp_ajax_nopriv_nbt_search_now', array($this, 'nbt_search_now') );
		add_action( 'wp_ajax_nbt_search_now', array($this, 'nbt_search_now') );




		add_shortcode( 'nbt_search', array($this, 'shortcode_ajax_search'), 10, 1 );

		add_filter('get_search_form', array($this, 'get_search_form'), 9999, 1);
	}

	public function get_search_form(){
		ob_start();
		nbt_ajax_search();
		return ob_get_clean();
	}

	public function shortcode_ajax_search(){
		nbt_ajax_search();
	}

	public function nbt_search_now(){
		global $wpdb, $post;
		$search = $_REQUEST['search'];
		if($search){
			$results = $wpdb->get_results($wpdb->prepare("SELECT * FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'product' AND (post_title LIKE %s OR post_content LIKE %s OR post_excerpt LIKE %s)", '%'.$wpdb->esc_like($search).'%', '%'.$wpdb->esc_like($search).'%', '%'.$wpdb->esc_like($search).'%'), OBJECT );
			if($results){
				$json['result'] = '<ul class="nbt-rs-lists">';
				foreach ($results as $key => $post) {
					$product = wc_get_product($post->ID);
					ob_start();
					?>
					<li>
						<div class="nbts-thumb"><?php echo $product->get_image( array( 70, 70 ) );?></div>
						<div class="nbts-info">
							<h3><a href="<?php echo get_permalink($post->ID );?>" title="<?php echo $post->post_title;?>"><?php echo $post->post_title;?></a></h3>
							<?php
							if ( 'no' === get_option( 'woocommerce_enable_review_rating' ) ) {
								return;
							}

							$rating_count = $product->get_rating_count();
							$review_count = $product->get_review_count();
							$average      = $product->get_average_rating();

							if ( $rating_count > 0 ) : ?>
								<div class="woocommerce-product-rating">
									<?php echo wc_get_rating_html( $average, $rating_count ); ?>
								</div>
							<?php else: ?>
								<div class="woocommerce-product-rating">
									<div class="star-rating" title="Rated 0.00 out of 5"><span style="width:0"><strong class="rating">0.00</strong> out of 5</span></div>
								</div>
							<?php endif; ?>
							<?php
					        if($product->get_price_html() !=''):
					            echo '<span class="product-price sr-price">';
					            echo $product->get_price_html();
					            echo '</span>';
					        endif;
					        ?>
						</div>
					</li>
					<?php
					$json['result'] .= ob_get_clean();
				}
				$json['result'] .= '</ul>';
			}else{
				$json['result'] = '<p class="nbt-no-result">'.__('No results.', 'nbt-solutions').'</p>';
			}
			$json['complete'] = true;
			

		}
		echo wp_json_encode($json, TRUE);
		wp_die();
	}


	function embed_style(){
		if( !defined('PREFIX_NBT_SOL') ){
			wp_enqueue_style( 'nbt-fonts-frontend', AJAX_SEARCH_URL .'assets/css/nbt-fonts.css', array(), '20160615' );
		}
		

		wp_enqueue_style( 'jquery.mCustomScrollbar', AJAX_SEARCH_URL . 'assets/css/jquery.mCustomScrollbar.min.css',false,'1.1','all');
		if( !defined('PREFIX_NBT_SOL') ){
			wp_enqueue_style( 'ajaxsearch-frontends', AJAX_SEARCH_URL . 'assets/css/frontend.css',false, false,'all');
		}


		wp_enqueue_script( 'jquery.mCustomScrollbar', AJAX_SEARCH_URL . 'assets/js/jquery.mCustomScrollbar.min.js', null, null, true );
		if( !defined('PREFIX_NBT_SOL') ){
			wp_enqueue_script( 'ajaxsearch', AJAX_SEARCH_URL . 'assets/js/frontend.js', null, null, true );
		}

	}
}
new NBT_WooCommerce_AjaxSearch_Frontend();