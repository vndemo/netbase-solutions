<?php
class NBT_One_Step_Checkout_Settings{
	static $id = 'one_step_checkout';

	protected static $initialized = false;

	public static function initialize() {
		// Do nothing if pluggable functions already initialized.
		if ( self::$initialized ) {
			return;
		}


		// State that initialization completed.
		self::$initialized = true;
	}

    public static function get_settings() {
        $settings = array(
            'redirect_cart' => array(
                'name' => __( 'Cart Page', 'nbt-ajax-cart' ),
                'type' => 'checkbox',
                'id'   => 'wc_'.self::$id.'_redirect_cart',
                'default' => false,
                'label' => 'Redirect Cart page to One Step Checkout page'
            ),
            'redirect_checkout' => array(
                'name' => __( 'Checkout Page', 'nbt-ajax-cart' ),
                'type' => 'checkbox',
                'id'   => 'wc_'.self::$id.'_redirect_checkout',
                'default' => false,
                'label' => 'Redirect Checkout page to One Step Checkout page'
            )
        );
        return apply_filters( 'nbt_'.self::$id.'_settings', $settings );
    }

}
