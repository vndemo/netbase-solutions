<?php
class NBT_OSC_Frontend {
	/**
	 * Class constructor.
	 */
	public function __construct() {
		add_shortcode('one_step_checkout', array($this, 'add_shortcode_one_step_checkout') );
		//add_filter( 'woocommerce_add_to_cart_redirect', array($this, 'custom_add_to_cart_redirect') );
		//add_filter( 'woocommerce_get_checkout_url', array($this, 'custom_add_to_cart_redirect'), 9999, 1 );
		add_filter( 'woocommerce_get_cart_url', array($this, 'custom_add_to_cart_redirect'), 9999, 1 );
		//add_filter( 'woocommerce_get_cart_page_permalink', array($this, 'custom_add_to_cart_redirect'), 9999, 1 );

		remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display');
		remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cart_totals');

		add_filter( 'woocommerce_coupons_enabled', array($this, 'hide_coupon_field_on_checkout') );

		add_filter( 'body_class', array($this, 'nbt_body_classes'), 10, 1 );

		

		add_action('wp_enqueue_scripts', array($this, 'embed_style'));

		add_action('woocommerce_before_checkout_form', array($this, 'woocommerce_before_checkout_form'));

		$GLOBAL['checkout_url'] = wc_get_checkout_url();


	}

	public function woocommerce_before_checkout_form(){
		echo do_shortcode('[woocommerce_cart]' );
	}
	public function nbt_body_classes( $classes ) {
		global $post;

		if(isset($post) && class_exists('NBT_Solutions_One_Step_Checkout') == $post->ID){
			$classes[] = 'woocommerce-checkout';
			$classes[] = 'wc-one-step-checkout';
		}
	     
	    return $classes;  
	}
	public function custom_add_to_cart_redirect($order_id){
		global $woocommerce;
		return wc_get_checkout_url();
	}
	public function add_shortcode_one_step_checkout(){
		echo do_shortcode('[woocommerce_cart]' );
		echo do_shortcode('[woocommerce_checkout]' );
	}

	public function embed_style(){
		global $post;
		if(isset($post) && is_checkout()){
			wp_enqueue_style('select2');
			
			wp_enqueue_script('wc-checkout');
			wp_enqueue_script('select2');

		}
	}

	public function hide_coupon_field_on_checkout(){
		return false;
	}

}
new NBT_OSC_Frontend();