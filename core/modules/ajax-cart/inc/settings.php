<?php
class NBT_Ajax_Cart_Settings{
	static $id = 'ajax_cart';

	protected static $initialized = false;

	public static function initialize() {
		// Do nothing if pluggable functions already initialized.
		if ( self::$initialized ) {
			return;
		}


		// State that initialization completed.
		self::$initialized = true;
	}

    public static function get_settings() {
        $settings = array(
            'icon' => array(
                'name' => __( 'Cart Icons', 'nbt-ajax-cart' ),
                'type' => 'icon',
                'desc'     => __( 'Start typing the Custom Tab name, Used for including custom tabs on all products.', 'GWP' ),
                'desc_tip' => true,
                'default' => 'nbt-icon-basket-4',
                'id'   => 'wc_'.self::$id.'_icon',
                'options' => NBT_Solutions_Ajax_Cart::set_ajaxcart_icon()
            ),
            'icon_color' => array(
                'name' => __( 'Color Icons', 'nbt-ajax-cart' ),
                'type' => 'color',
                'id'   => 'wc_'.self::$id.'_color_icon',
                'default' => '#1d242a'
            ),
            'color_count' => array(
                'name' => __( 'Color Count', 'nbt-ajax-cart' ),
                'type' => 'color',
                'id'   => 'wc_'.self::$id.'_color_count',
                'default' => '#e60000'
            ),
            'count_color_text' => array(
                'name' => __( 'Color Count Text', 'nbt-ajax-cart' ),
                'type' => 'color',
                'id'   => 'wc_'.self::$id.'_color_count_text',
                'default' => '#fff'
            ),
            'primary_color' => array(
                'name' => __( 'Primary Color', 'nbt-ajax-cart' ),
                'type' => 'color',
                'id'   => 'wc_'.self::$id.'_primary_color',
                'default' => '#25bce9'
            ),
            'top' => array(
                'name' => __( 'Top Notification', 'nbt-ajax-cart' ),
                'type' => 'number',
                'id'   => 'wc_'.self::$id.'_top',
                'default' => '40',
                'min' => 0,
                'max' => 200,
                'step' => 1
            )
        );
        return apply_filters( 'nbt_'.self::$id.'_settings', $settings );
    }

}
