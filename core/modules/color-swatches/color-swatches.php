<?php
/**
 * @version    1.0
 * @package    Package Name
 * @author     Your Team <support@yourdomain.com>
 * @copyright  Copyright (C) 2014 yourdomain.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 */

/**
 * Plug additional sidebars into WordPress.
 *
 * @package  Package Name
 * @since    1.0
 */
define('NBT_CS_PATH', plugin_dir_path( __FILE__ ));
define('NBT_CS_URL', plugin_dir_url( __FILE__ ));

class NBT_Solutions_Color_Swatches {
    /**
     * Variable to hold the initialization state.
     *
     * @var  boolean
     */
    protected static $initialized = false;

    public static $types = array();
    
    /**
     * Initialize functions.
     *
     * @return  void
     */
    public static function initialize() {
        // Do nothing if pluggable functions already initialized.
        if ( self::$initialized ) {
            return;
        }

        if ( ! function_exists( 'WC' ) ) {
            add_action( 'admin_notices', array( __CLASS__, 'install_woocommerce_admin_notice') );
        }else{
            self::$types = array(
                'color' => esc_html__( 'Color', 'wcvs' ),
                'image' => esc_html__( 'Image', 'wcvs' ),
                'radio' => esc_html__( 'Radio', 'wcvs' ),
            );

            add_action( 'init', array( __CLASS__, 'load_textdomain' ) );
            add_filter( 'product_attributes_type_selector', array( __CLASS__, 'add_attribute_types' ) );


            if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
                require_once 'inc/ajax.php';
                NBT_Color_Swatches_Ajax::initialize();
            }
            
            require_once 'inc/admin.php';
            require_once 'inc/frontend.php';
        }
        // Register actions to do something.
        //add_action( 'action_name'   , array( __CLASS__, 'method_name'    ) );
        // State that initialization completed.
        self::$initialized = true;
    }
    
    /**
     * Method Featured.
     *
     * @return  array
     */
    public static function install_woocommerce_admin_notice() {?>
        <div class="error">
            <p><?php _e( 'WooCommerce plugin is not activated. Please install and activate it to use for plugin <strong>NBT WooCommerce Price Matrix</strong>.', 'nbt-ajax-cart' ); ?></p>
        </div>
        <?php    
    }


    /**
     * Load plugin text domain
     */
    public static function load_textdomain() {
        load_plugin_textdomain( 'wcvs', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
    }

    /**
     * Add extra attribute types
     * Add color, image and label type
     *
     * @param array $types
     *
     * @return array
     */
    public static function add_attribute_types( $types ) {
        $types = array_merge( $types, self::$types );

        return $types;
    }

    /**
     * Get attribute's properties
     *
     * @param string $taxonomy
     *
     * @return object
     */
    public static function get_tax_attribute( $taxonomy ) {
        global $wpdb;

        $attr = substr( $taxonomy, 3 );
        $attr = $wpdb->get_row( "SELECT * FROM " . $wpdb->prefix . "woocommerce_attribute_taxonomies WHERE attribute_name = '$attr'" );

        return $attr;
    }

    public static function get_style(){
        return array(
            'square' => __('Square', 'nbtcs'),
            'circle' => __('Circle', 'nbtcs')

        );
    }
}