<?php
class NBT_LCHAT_Frontend {
	protected $currency = array();
	protected $current_currency = '';
	/**
	 * Class constructor.
	 */
	public function __construct() {
		add_action('wp_footer', array($this, 'embed_code_live_chat'));

	}

	public function embed_code_live_chat(){
		$data = get_option(NBT_Solutions_Live_Chat::$plugin_id.'_settings');
		if(!empty($data['nbt_live-chat_embed_code']) && $data['nbt_live-chat_always_show']){
			echo str_replace('\\', '', $data['nbt_live-chat_embed_code']);
		}
	}

}
new NBT_LCHAT_Frontend();