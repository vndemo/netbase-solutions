<link rel='stylesheet' id='media-views-css'  href='<?php echo home_url();?>/wp-includes/css/media-views.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='media-views-css'  href='<?php echo home_url();?>/wp-admin/css/media.css?ver=4.8' type='text/css' media='all' />

<?php $title = __('Order Upload Media');?>
	<div class="wrap order-upload-library" id="wp-media-grid" data-search="<?php _admin_search_query() ?>">
		<h1 class="wp-heading-inline"><?php echo esc_html( $title ); ?></h1>

		<?php
		if ( current_user_can( 'upload_files' ) ) { ?>
			<a href="<?php echo admin_url( 'media-new.php' ); ?>" class="page-title-action aria-button-if-js"><?php echo esc_html_x( 'Add New', 'file' ); ?></a><?php
		}
		?>

		<hr class="wp-header-end">

		<div class="error hide-if-js">
			<p><?php printf(
				/* translators: %s: list view URL */
				__( 'The grid view for the Media Library requires JavaScript. <a href="%s">Switch to the list view</a>.' ),
				'admin.php?page=order-upload&mode=list'
			); ?></p>
		</div>

		<div class="media-frame wp-core-ui mode-grid mode-edit hide-menu">
			<div class="media-frame-content" data-columns="8">
				<div class="media-toolbar wp-filter">
				   <div class="media-toolbar-left">
				      <input type="text" name="search_order" class="search-order" placeholder="Search order number...">
				      <button type="button" class="button media-button  select-mode-toggle-button">Filter</button><span class="spinner"></span>
				   </div>
				</div>
				<ul class="attachments ui-sortable ui-sortable-disabled">
				</ul>
				
			</div>


		</div>
	</div>



<script type="text/javascript">
	jQuery(document).ready(function($){
		$(window).load(function(){
			$('.spinner').addClass('is-active');

	
			$.ajax({
				url: '<?php echo admin_url('admin-ajax.php');?>',
				data: {
					action: 'ou_load_media'
				},
				type: 'POST',
				datatype: 'json',
				success: function( response ) {
					var rs = JSON.parse(response);

					if(rs.complete != undefined){
						$('.attachments').html(rs.return);
					}
					$('.spinner').removeClass('is-active');

				},
				error:function(){
					alert('There was an error when processing data, please try again !');
					$('.spinner').removeClass('is-active');
				}
			});

		});

		$(document).on('click', '.attachment-preview', function(){
			$('body').addClass('upload-php');
			$('.spinner').addClass('is-active');
			$('.overlay-gallery').attr('data-id', $(this).attr('data-id'));
			$.ajax({
				url: '<?php echo admin_url('admin-ajax.php');?>',
				data: {
					action: 'load_image',
					attachment_id: $(this).attr('data-id')
				},
				type: 'POST',
				datatype: 'json',
				success: function( response ) {
					var rs = JSON.parse(response);
					$('.attachment-details').html(rs.content);
					$('.overlay-gallery').show();
					$('.spinner').removeClass('is-active');



				},
				error:function(){
					alert('There was an error when processing data, please try again !');
					$('.spinner').removeClass('is-active');
				}
			});
		});

		$(document).on('click', '.right.dashicons', function(){
			$.ajax({
				url: '<?php echo admin_url('admin-ajax.php');?>',
				data: {
					action: 'load_image',
					attachment_id: $('.overlay-gallery').attr('data-id'),
					direction: 'next'
				},
				type: 'POST',
				datatype: 'json',
				success: function( response ) {
					var rs = JSON.parse(response);
					$('.attachment-details').html(rs.content);
					$('.overlay-gallery').show();
					$('.spinner').removeClass('is-active');
					$('.overlay-gallery').attr('data-id', rs.id);

				},
				error:function(){
					alert('There was an error when processing data, please try again !');
					$('.spinner').removeClass('is-active');
				}
			});
		});
		
		$(document).on('click', '.left.dashicons', function(){
			$.ajax({
				url: '<?php echo admin_url('admin-ajax.php');?>',
				data: {
					action: 'load_image',
					attachment_id: $('.overlay-gallery').attr('data-id'),
					direction: 'prev'
				},
				type: 'POST',
				datatype: 'json',
				success: function( response ) {
					var rs = JSON.parse(response);
					$('.attachment-details').html(rs.content);
					$('.overlay-gallery').show();
					$('.spinner').removeClass('is-active');
					$('.overlay-gallery').attr('data-id', rs.id);

				},
				error:function(){
					alert('There was an error when processing data, please try again !');
					$('.spinner').removeClass('is-active');
				}
			});
		});

		$(document).on('click', '.media-button', function(){
			$('.spinner').addClass('is-active');
			$.ajax({
				url: '<?php echo admin_url('admin-ajax.php');?>',
				data: {
					action: 'filter_upload',
					search: $('.search-order').val()
				},
				type: 'POST',
				datatype: 'json',
				success: function( response ) {
					var rs = JSON.parse(response);
					if(rs.complete != undefined){
						$('.attachments').html(rs.return);
					}
					$('.spinner').removeClass('is-active');




				},
				error:function(){
					alert('There was an error when processing data, please try again !');
					$('.spinner').removeClass('is-active');
				}
			});
		});

	});
</script>


<div class="overlay-gallery" style="display: none">
		<div class="media-modal wp-core-ui">
			<button type="button" class="media-modal-close"><span class="media-modal-icon"><span class="screen-reader-text">Close media panel</span></span></button>
			<div class="media-modal-content"><div class="edit-attachment-frame mode-select hide-menu hide-router">
		<div class="edit-media-header">
			<button class="left dashicons"><span class="screen-reader-text">Edit previous media item</span></button>
			<button class="right dashicons"><span class="screen-reader-text">Edit next media item</span></button>
		</div>
		<div class="media-frame-title"><h1>Attachment Details</h1></div>
		<div class="media-frame-content">
	<div tabindex="0" data-id="95" class="attachment-details save-ready">
		Loading...
	</div></div>
	</div></div>
		</div>
		<div class="media-modal-backdrop"></div>
	</div>
</div>