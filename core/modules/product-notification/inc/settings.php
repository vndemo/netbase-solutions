<?php
class NBT_Product_Notification_Settings{
	static $id = 'product_notification';

	protected static $initialized = false;

	public static function initialize() {
		// Do nothing if pluggable functions already initialized.
		if ( self::$initialized ) {
			return;
		}


		// State that initialization completed.
		self::$initialized = true;
	}

    public static function get_settings() {
        $settings = array(
            array(
                'name' => __( 'Heading', 'nbt-solutions' ),
                'type' => 'text',
                'id'   => 'nbt_'.self::$id.'_title',
                'default' => 'Oh my...'
            ),
            array(
                'name' => __( 'Description', 'nbt-solutions' ),
                'type' => 'textarea',
                'id'   => 'nbt_'.self::$id.'_desc',
                'default' => 'It seems this one is sold out. Enter your mail and get notified with a gift when it\'s back in stock!'
            ),
            array(
                'name' => __( 'Label of button', 'nbt-solutions' ),
                'type' => 'text',
                'id'   => 'nbt_'.self::$id.'_button',
                'default' => 'Keep me updated'
            ),
            array(
                'name' => __( 'Label click', 'nbt-solutions' ),
                'type' => 'text',
                'id'   => 'nbt_'.self::$id.'_change_email',
                'default' => 'Click here to change your email address'
            ),
            array(
            	'type' => 'border'
            ),
            array(
                'name' => __( 'Email title', 'nbt-solutions' ),
                'type' => 'text',
                'id'   => 'nbt_'.self::$id.'_email_title',
                'default' => 'Product Notification: %product%'
            ),
            array(
                'name' => __( 'Email content', 'nbt-solutions' ),
                'type' => 'textarea',
                'id'   => 'nbt_'.self::$id.'_email_desc',
                'default' => 'It seems this one is sold out. Enter your mail and get notified with a gift when it\'s back in stock!',
                'rows' => 10,
                'desc_tip' => '<p><strong>%product%</strong>: Show product title</p>'
            ),
            array(
                'name' => __( 'Re-captcha Sitekey', 'nbt-solutions' ),
                'type' => 'text',
                'id'   => 'nbt_'.self::$id.'_sitekey',
                'default' => ''
            ),
            array(
                'type' => 'border'
            ),
            array(
                'name' => __( 'Re-captcha Secret', 'nbt-solutions' ),
                'type' => 'text',
                'id'   => 'nbt_'.self::$id.'_secret',
                'default' => ''
            ),
        );
        return apply_filters( 'nbt_'.self::$id.'_settings', $settings );
    }

}
