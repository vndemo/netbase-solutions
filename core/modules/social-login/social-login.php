<?php
/*
  Plugin Name: NBT Social Login
  Plugin URI: http://netbaseteam.com
  Description: Change the default behavior of WooCommerce Cart page, making AJAX requests when quantity field changes
  Version: 1.5.1
  Author: Netbaseteam
  Author URI: ttps://netbaseteam.com/
 */
define('SOCIAL_LOGIN_PATH', plugin_dir_path( __FILE__ ));
define('SOCIAL_LOGIN_URL', plugin_dir_url( __FILE__ ));

class NBT_Solutions_Social_Login {
    /**
     * Variable to hold the initialization state.
     *
     * @var  boolean
     */
    protected static $initialized = false;
    
    /**
     * Initialize functions.
     *
     * @return  void
     */
    public static function initialize() {
        // Do nothing if pluggable functions already initialized.
        if ( self::$initialized ) {
            return;
        }
        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        if ( ! is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
            add_action( 'admin_notices', array( __CLASS__, 'install_woocommerce_admin_notice') );
        }else{
            if( !class_exists('NBT_WooCommerce_Social_Login_Admin') ){
               require_once( SOCIAL_LOGIN_PATH . '/inc/admin.php' ); 
            }
            require_once( SOCIAL_LOGIN_PATH . '/inc/frontend.php' );
        }
        

        // State that initialization completed.
        self::$initialized = true;
    }

}
