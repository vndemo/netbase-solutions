<?php
class NBT_Social_Login_Settings{
	static $id = 'social_login';

	protected static $initialized = false;

	public static function initialize() {
		// Do nothing if pluggable functions already initialized.
		if ( self::$initialized ) {
			return;
		}


		// State that initialization completed.
		self::$initialized = true;
	}

    public static function get_settings() {
        $settings = array(
            'facebook_id' => array(
                'name' => __( 'Facebook ID', 'nbt-social-login.css' ),
                'type' => 'text',
                'id'   => 'nbt_'.self::$id.'_facebook_id',
                'default' => ''
            ),
            'facebook_secret' => array(
                'name' => __( 'Facebook Secret', 'nbt-social-login.css' ),
                'type' => 'text',
                'id'   => 'nbt_'.self::$id.'_facebook_secret',
                'default' => ''
            ),

            'google_id' => array(
                'name' => __( 'Google+ ID', 'nbt-social-login.css' ),
                'type' => 'text',
                'id'   => 'nbt_'.self::$id.'_google_id',
                'default' => ''
            ),

            'google_secret' => array(
                'name' => __( 'Google+ Secret', 'nbt-social-login.css' ),
                'type' => 'text',
                'id'   => 'nbt_'.self::$id.'_google_secret',
                'default' => ''
            ),
        );
        return apply_filters( 'nbt_'.self::$id.'_settings', $settings );
    }

}
