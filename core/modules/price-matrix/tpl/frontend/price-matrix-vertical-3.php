<?php
$attribute_one = pm_attribute_label($_pm_attr['vertical'][0], $product->get_id());
$attribute_two = pm_attribute_label($_pm_attr['vertical'][1], $product->get_id());
$attribute_three = pm_attribute_label($_pm_attr['horizontal'][0], $product->get_id());
?>

<div class="table-responsive">
    <table class="pure-table price-matrix-table vertical-3">
        <tbody>
            <tr>
                <td class="attr-name colspan" colspan="2"></td>
                <?php foreach ($attribute_three as $kat => $attr_three) :?>
                <td class="attr-name heading-center"><?php echo $attr_three->name;?></td>
                <?php endforeach;?>
            </tr>
    		<?php
    		foreach ($attribute_one as $key => $attr_one) :
    			foreach ($attribute_two as $katw => $attr_two) :?>
    	        <tr>
    	        	<?php if($katw == 0):?>
    	            <td class="attr-name first" rowspan="<?php echo count($attribute_two);?>"><?php echo $attr_one->name;?></td>
    	            <?php endif;?>
    	            <td class="attr-name"><?php echo $attr_two->name;?></td>
    	            <?php foreach ($attribute_three as $kat => $attr_three):
                    $hover_detail = '';
    	            $group_attr = array(
                		array(
                			'name' => $attr_one->taxonomy,
                			'value' => $attr_one->slug
                		),
                		array(
                			'name' => $attr_two->taxonomy,
                			'value' => $attr_two->slug
                		),
                		array(
                			'name' => $attr_three->taxonomy,
                			'value' => $attr_three->slug
                		)
                	);

                    if($deprived){
                        $group_attr = array_merge($group_attr, $deprived);
                    }

                    $hover_detail .= '<tr><td>'.pm_attribute_tax($attr_one->taxonomy, $product->get_id()).'</td><td>'.$attr_one->name.'</td></tr>';
                    $hover_detail .= '<tr><td>'.pm_attribute_tax($attr_two->taxonomy, $product->get_id()).'</td><td>'.$attr_two->name.'</td></tr>';
                    $hover_detail .= '<tr><td>'.pm_attribute_tax($attr_three->taxonomy, $product->get_id()).'</td><td>'.$attr_three->name.'</td></tr>';
    	            ?>
    	            <td class="price tippy" title="<table><?php echo $hover_detail;?></table>" data-attr="<?php echo htmlspecialchars(wp_json_encode( $group_attr ));?>"><span class="pm-price-wrap"><?php echo pm_attribute_price($group_attr, $product->get_id());?></span></td>
    	            <?php endforeach;?>
    	        </tr>
            	<?php endforeach;
        	endforeach;
            ?>
        </tbody>
    </table>
</div>