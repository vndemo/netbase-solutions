<?php
$attribute_one = pm_attribute_label($_pm_attr['vertical'][0], $product->get_id());
$attribute_two = pm_attribute_label($_pm_attr['horizontal'][0], $product->get_id());

?>
<div class="table-responsive<?php if($khuyet){ echo ' hide';}?>">
    <table class="pure-table price-matrix-table">
        <tbody>
            <tr>
                <td class="attr-name"></td>
                <?php foreach ($attribute_one as $kat => $attr_one) :?>
                <td class="attr-name heading-center"><?php echo $attr_one->name;?></td>
                <?php endforeach;?>
            </tr>
            <?php foreach ($attribute_two as $k_two => $attr_two) :?>
            <tr>
                <td class="attr-name"><?php echo $attr_two->name;?></td>
                <?php foreach ($attribute_one as $kat => $attr_one) :
                    $hover_detail = '';
                    $group_attr = array(
                        array(
                            'name' => $attr_one->taxonomy,
                            'value' => $attr_one->slug
                        ),
                        array(
                            'name' => $attr_two->taxonomy,
                            'value' => $attr_two->slug
                        )
                    );

                    if($deprived){
                        $group_attr = array_merge($group_attr, $deprived);
                    }

                    $hover_detail .= '<tr><td>'.pm_attribute_tax($attr_one->taxonomy, $product->get_id()).'</td><td>'.$attr_one->name.'</td></tr>';
                    $hover_detail .= '<tr><td>'.pm_attribute_tax($attr_two->taxonomy, $product->get_id()).'</td><td>'.$attr_two->name.'</td></tr>';
         $show_regular_price = '';
        if(isset($pm_settings['wc_price-matrix_show_sales']) && $pm_settings['wc_price-matrix_show_sales'] == 1){
            $show_regular_price = ' show';
        }
                    ?>
                    <td class="price tippy" title="<table><?php echo $hover_detail;?></table>" data-attr="<?php echo htmlspecialchars(wp_json_encode( $group_attr ));?>"><span class="pm-price-wrap<?php echo $show_regular_price;?>"><?php echo pm_attribute_price($group_attr, $product->get_id());?></span></td>
                <?php endforeach;?>
            </tr>
        <?php endforeach;?>
        </tbody>
    </table>
</div>