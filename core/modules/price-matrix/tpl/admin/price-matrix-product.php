<style>
._pm_show_on_field{
	margin-top: 0 !important
}
</style>
<div id="price_matrix" class="panel wc-metaboxes-wrapper woocommerce_options_panel hidden">
	<div id="price_matrix_options_inner" data-count="<?php echo $count_attr;?>">



		<?php
		$_product_attributes = get_post_meta($post->ID, '_product_attributes', TRUE);
		if($count_attr >= 2){?>
		<div id="price_matrix_table">
			<form action="" method="POST" id="frm-price-matrix">
				<table class="pm_repeater">
					<thead>
						<tr>
							<th class="pm-row-zero"></th>
							<th class="pm-th"><?php _e('Attributes', 'nbt-wocommerce-price-matrix');?></th>
							<th class="pm-th"><?php _e('Direction', 'nbt-wocommerce-price-matrix');?></th>
							<th class="pm-row-zero"></th>
						</tr>
					</thead>

					<tbody>
						<?php
						if(isset($_pm_table_attr)){
							foreach ($_pm_table_attr as $key => $v_attr) {
								if(isset($_product_attributes[$v_attr])){?>
								<tr class="pm-row">
									<td class="pm-row-zero order">
										<span><?php echo ($key+1);?></span>
									</td>

									<td class="pm-field">
										<div class="pm-input">
											<div class="pm-input-wrap">
												<select class="pm-attributes-field" name="pm_attr[]" data-value="<?php echo $v_attr;?>">
													<option value="0">(Select an Attributes)</option>
												</select>
												<input type="text" name="attributes_hidden[]" class="attributes_hidden" style="display: none" />
											</div>
										</div>
									</td>
									<td class="pm-field">
										<div class="pm-input">
											<div class="pm-input-wrap">
												<select class="pm-direction-field" name="pm_direction[]" data-value="<?php echo $_pm_table_direction[$key];?>">
													<option value="vertical">Vertical</option>
													<option value="horizontal">Horizontal</option>
												</select>
											</div>
										</div>
									</td>
									<td class="pm-row-zero">
										<a class="pm-icon -plus small" href="#" data-event="add-row" title="Add row"></a>
										<a class="pm-icon -minus small" href="#" data-event="remove-row" title="Remove row"></a>
									</td>
								</tr>
							<?php }
							}
						}?>
					</tbody>
				</table>

				<input type="hidden" name="security" value="<?php echo wp_create_nonce( "_price_matrix_save" );?>" />
				<button type="button" class="button save_price_matrix button-primary" style="margin-top: 15px;">Save</button>
				<button type="button" class="button btn-enter-price">Input Price</button>
			</form>
		</div>

		<?php }?>











<style>

#pm-live-table{
	margin: 0 10px;
}
#pm-live-table table {
    max-width: 100%;
    background-color: transparent;
    border-collapse: collapse;
    border-spacing: 0;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    outline: 0;
    margin: 10px 0;
    border: none!important;
}

.pm-live-table-view td {
    border: 1px #aaa dotted;
    font-size: 16px;
    font-family: serif;
    vertical-align: top;
    box-sizing: border-box;
    min-width: 20px;
    cursor: default;
    padding: 0!important;
}
.pm-live-table-view .pm-cell {
    background-color: transparent!important;
    border: 1px transparent solid;
    border-right: 0 #ddd solid;
    border-bottom: 0 #ddd solid;
    text-align: center!important;
    font-family: Arial,sans-serif;
    color: #999!important;
    font-size: 90%;
    font-weight: 400!important;
    vertical-align: middle;
    user-select: none;
}
.pm-live-table-view td.pm-cell-selected:not(.un-pm-cell) {
    background-color: #eee!important;
}
.pm-live-table-view td.selected .wraps {
    margin-top: -10px;
    padding-top: 10px;
    background-color: khaki;
    background-color: rgba(240,230,140,.5)!important;
    margin-bottom: -99999px;
    padding-bottom: 99999px;
}
.pm-live-table-view td.selected {
    overflow: hidden;
}
.pm-live-table-view td .wraps div {
    margin: 10px 5px;
    line-height: 1.3em;
    min-height: 20px;
    min-width: 10px;
    outline-style: solid;
    outline-width: 0;
    outline-color: #aaa;
}
.pm-live-table-view td .wraps .entry-editing {
    -webkit-touch-callout: text;
    -webkit-user-select: text;
    -khtml-user-select: text;
    -moz-user-select: text;
    -ms-user-select: text;
    user-select: text;
    outline-width: 1px;
    cursor: text;
}
.attributes_hidden{
	display: none !important;
}

</style>



	</div>
</div>

<script id="msg-js" type="text/template">
	<div id="message" class="inline notice woocommerce-message">
		<p><?php _e( 'Before you can use Price Matrix, you need add minimum of two variation attributes on the <strong>Attributes</strong> tab.', 'woocommerce' ); ?></p>
		<p><a class="button-primary" href="<?php echo esc_url( apply_filters( 'woocommerce_docs_url', 'https://docs.woocommerce.com/document/variable-product/', 'product-variations' ) ); ?>" target="_blank"><?php _e( 'Learn more', 'woocommerce' ); ?></a></p>
	</div>
</script>

<script id="temp-repeater" type="text/template">

	<div id="price_matrix_table">
		<form action="" method="POST" id="frm-price-matrix">
			<table class="pm_repeater">
				<thead>
					<tr>
						<th class="pm-row-zero"></th>
						<th class="pm-th"><?php _e('Attributes', 'nbt-wocommerce-price-matrix');?></th>
						<th class="pm-th"><?php _e('Direction', 'nbt-wocommerce-price-matrix');?></th>
						<th class="pm-row-zero"></th>
					</tr>
				</thead>

				<tbody>
					<tr class="pm-row">
						<td class="pm-row-zero order">
							<span>1</span>
						</td>

						<td class="pm-field">
							<div class="pm-input">
								<div class="pm-input-wrap">
									<select class="pm-attributes-field" name="pm_attr[]">
										<option value="0">(Select an Attributes)</option>
									</select>
									<input type="text" name="attributes_hidden[]" class="attributes_hidden" style="display: none" />
								</div>
							</div>
						</td>
						<td class="pm-field">
							<div class="pm-input">
								<div class="pm-input-wrap">
									<select class="pm-direction-field" name="pm_direction[]">
										<option value="vertical">Vertical</option>
										<option value="horizontal">Horizontal</option>
									</select>
								</div>
							</div>
						</td>
						<td class="pm-row-zero">
							<a class="pm-icon -plus small" href="#" data-event="add-row" title="Add row"></a>
							<a class="pm-icon -minus small" href="#" data-event="remove-row" title="Remove row"></a>
						</td>
					</tr>
				</tbody>
			</table>

			<input type="hidden" name="security" value="<?php echo wp_create_nonce( "_price_matrix_save" );?>" />
			<button type="button" class="button save_price_matrix button-primary" style="margin-top: 15px;">Save</button>
			<button type="button" class="button btn-enter-price" disabled>Input Price</button>
		</form>
	</div>
</script>