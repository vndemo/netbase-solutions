jQuery( function( $ ) {
	
	var $el = $( '.variations_form');

	var pm_load = {
		/**
		 * Init jQuery.BlockUI
		 */
		block: function() {
			$el.block({
				message: null,
				overlayCSS: {
					background: '#fff',
					opacity: 0.6
				}
			});
		},

		/**
		 * Remove jQuery.BlockUI
		 */
		unblock: function() {
			$el.unblock();
		}
	}
	var pm_frontend = {

		/**
		 * Initialize variations actions
		 */
		init: function() {
        	$(document).on('click', '.pure-table .price', this.selected_price);
        	$(document).on('change', 'table.un-variations tr:visible select', this.change_attr);
     

            Tippy('.tippy', {
                animation: 'scale',
                duration: 200,
                arrow: true,
                position: 'bottom'
            });
		},
		load_var: function(){
			
			// var $list_attr = $('#price_attr').val();
			// $( "table.variations select" ).each(function( index ) {
			// 	var $this = $(this);
			// 	var $id = $(this).attr('id');
				
			// 	$.each(JSON.parse($list_attr), function (key, pm) {
			// 		if(pm == $id){
			// 			$this.closest('tr').hide();

			// 		}
			// 	});
			// });
			// if($( "table.variations select" ).length > JSON.parse($list_attr).length){
			// 	$('.price-matrix-table').hide();
			// }
			// if($('.variations').height() == 0){
			// 	$('.variations').hide();
			// }
		},
		change_attr: function(){
			var $this = $(this).val();
			pm_load.block();
			var $total = $( 'table.un-variations tr:visible select' ).length;

			var optionVal = {};
			var count = 0;
			$( 'table.un-variations tr:visible select' ).each(function( index ) {
				if($(this).val()){
					var $id = $(this).closest('select').attr('id');
					var $val = $(this).val();

					optionVal[$id] = $val;
					count += 1;
				}
			});



			if($this && $total == count){
				pm_load.block();
				$.ajax({
					url: wc_add_to_cart_params .ajax_url,
					data: {
						action:     'pm_load_matrix',
						security:   $('[name="security"]').val(),
						product_id: $('[name="add-to-cart"]').val(),
						attr: optionVal
					},
					type: 'POST',
					datatype: 'json',
					success: function( response ) {
						var rs = JSON.parse(response);
						
						if(rs.complete != undefined){
							$('.price_attr').remove();
							$('.table-responsive, [name="price_attr"], [name="security"]').remove();
							$('table.un-variations').after(rs.return);

				            Tippy('.tippy', {
				                animation: 'scale',
				                duration: 200,
				                arrow: true,
				                position: 'bottom'
				            });
						}
						pm_load.unblock();

					},
					error:function(){
						alert('There was an error when processing data, please try again !');
					}
				});
			}else{
				$('.table-responsive').remove();
				pm_load.unblock();
			}


		},
		selected_price: function(){
			if( $('.pure-table td.price').not($(this)).hasClass('selected') ){
				$('.pure-table td.price.selected').removeAttr('style');
			}
			$('.pure-table .price').removeClass('selected');
			$(this).addClass('selected');
			
			var $attr = $(this).attr('data-attr');
			$.each(JSON.parse($attr), function (key, pm) {
			    $('#' + pm.name).val(pm.value);
			});
			$('table.variations tr:not(:visible) select').trigger('change');

			if($('body').hasClass('has-pm-scroll')){
				$('html,body').animate({
		        scrollTop: $(".woocommerce-variation-add-to-cart").offset().top - 100},
		        'slow');
			}
		    
		}
	}

	if($( ".pure-table" ).length > 0){
		pm_frontend.init();
		pm_frontend.load_var();
	}
	
});