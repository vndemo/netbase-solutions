<?php
class NBT_CS_Frontend {
	protected $currency = array();
	protected $current_currency = '';
	/**
	 * Class constructor.
	 */
	public function __construct() {
		add_action('wp_enqueue_scripts', array($this, 'embed_style'));
		add_shortcode('nbt_currency_switcher', array($this, 'add_nbt_currency_switcher') );
		if( !is_admin()){
			add_action( 'woocommerce_currency_symbol', array( $this, 'woocommerce_currency_symbol' ), 9999, 1 );
			add_filter('woocommerce_currency', array($this, 'get_woocommerce_currency'), 9999, 1);
			add_filter('raw_woocommerce_price', array($this, 'raw_woocommerce_price'), 9999);
			add_filter('woocommerce_price_format', array($this, 'woocommerce_price_format'), 9999, 2);
			add_filter( 'wc_price_args', array($this, 'filter_wc_price_args'), 9999, 1 );
			add_action('woocommerce_checkout_update_order_meta', array($this, 'woocommerce_checkout_update_order_meta'), 20, 2);
		}
		$settings_mcs = get_option(NBT_Solutions_Currency_Switcher::$plugin_id.'_settings');
		if(isset($settings_mcs['nbt_currency-switcher_repeater'])){
			$GLOBALS['cs'] = $settings_mcs['nbt_currency-switcher_repeater'];
			if(method_exists(WC()->session,'get')){
				$ss_wc_currency = WC()->session->get('wc_currency');

				$key = array_search($ss_wc_currency, array_column($GLOBALS['cs'], 'nbt_currency-switcher_repeater_currency'));

				if($ss_wc_currency && isset($GLOBALS['cs'][$key])){
					$GLOBALS['currency'] = $GLOBALS['cs'][$key];
					$GLOBALS['currency_symbol'] = $GLOBALS['cs'][$key]['nbt_currency-switcher_repeater_symbol'];
				}
			}
		}
		


	}

	public function woocommerce_checkout_update_order_meta($order_id, $data){
		if(method_exists(WC()->session,'get')){
			$ss_wc_currency = WC()->session->get('wc_currency');
			if($ss_wc_currency){
				$settings_mcs = get_option(NBT_Solutions_Currency_Switcher::$plugin_id.'_settings');
				$currency = $settings_mcs[$ss_wc_currency];
				$ajaxcart_rate = $currency['nbt_'.NBT_Solutions_Currency_Switcher::$plugin_id.'_rates'];
				$ajaxcart_position = $currency['nbt_'.NBT_Solutions_Currency_Switcher::$plugin_id.'_position'];
				$ajaxcart_decimals = $currency['nbt_'.NBT_Solutions_Currency_Switcher::$plugin_id.'_decimals'];
				$ajaxcart_symbol =get_woocommerce_currency_symbol( $ss_wc_currency );

				switch ($ajaxcart_position) {
				    case 'left' :
					$ajaxcart_pos = '%1$s%2$s';
					break;
				    case 'right' :
					$ajaxcart_pos = '%2$s%1$s';
					break;
				    case 'left_space' :
					$ajaxcart_pos = '%1$s&nbsp;%2$s';
					break;
				    case 'right_space' :
					$ajaxcart_pos = '%2$s&nbsp;%1$s';
					break;
				}
				$cs_array = array(
					'rate' => $ajaxcart_rate,
					'pos' => $ajaxcart_pos,
					'decimals' => $ajaxcart_decimals,
					'symbol' => $ajaxcart_symbol,
					'currency' => $ss_wc_currency
				);
				update_post_meta($order_id, '_currency_order', $cs_array);
			}
		}

	}

	public function filter_wc_price_args($array){
		global $currency;
		if(isset($currency['nbt_'.NBT_Solutions_Currency_Switcher::$plugin_id.'_decimals'])){
			$array['decimals'] = $currency['nbt_'.NBT_Solutions_Currency_Switcher::$plugin_id.'_decimals'];
		}
		return $array;
	}

	public function woocommerce_price_format($format, $currency_pos){
		global $currency;
		if(isset($currency) && !empty($currency)){
			$format = '%1$s%2$s';
			if(isset($currency['nbt_'.NBT_Solutions_Currency_Switcher::$plugin_id.'_position'])){
				switch ($currency['nbt_'.NBT_Solutions_Currency_Switcher::$plugin_id.'_position']) {
				    case 'left' :
					$format = '%1$s%2$s';
					break;
				    case 'right' :
					$format = '%2$s%1$s';
					break;
				    case 'left_space' :
					$format = '%1$s&nbsp;%2$s';
					break;
				    case 'right_space' :
					$format = '%2$s&nbsp;%1$s';
					break;
				}

				return apply_filters('nbt_price_format', $format, $currency['nbt_'.NBT_Solutions_Currency_Switcher::$plugin_id.'_position']);
			}
		}else{
			return apply_filters( 'nbt_price_format', $format, $currency_pos );
		}



	}
	public function raw_woocommerce_price($price, $product = NULL){
		global $currency;
		if(isset($currency['nbt_'.NBT_Solutions_Currency_Switcher::$plugin_id.'_rates'])){
			return $price * $currency['nbt_'.NBT_Solutions_Currency_Switcher::$plugin_id.'_rates'];
		}else{
			return $price;
		}
	}
	public function get_woocommerce_currency($currency){
		global $currency_symbol;
		if( isset($currency_symbol) && !empty($currency_symbol) ){
			$currency = $currency_symbol;
		}
		return $currency;	
	}
	public function woocommerce_currency_symbol($wc_currency){
		global $cs, $currency_symbol;

		if(isset($currency_symbol)){
			return $currency_symbol;
		}

		return $wc_currency;
	}


	public function add_nbt_currency_switcher(){
		if(!empty($_SERVER['QUERY_STRING'])){
			$touch = '?'.$_SERVER['QUERY_STRING'].'&';
		}else{
			$touch = '?';
		}
		global $wp;
		$current_url = home_url(add_query_arg(array(),$wp->request));
		$settings_mcs = get_option(NBT_Solutions_Currency_Switcher::$plugin_id.'_settings');
		$settings_mcs = $settings_mcs['nbt_currency-switcher_repeater'];
		$wc_currency = get_woocommerce_currency();
		$wc_position = get_option( 'woocommerce_currency_pos' );


	
		if(isset($_REQUEST['currency'])){
			$wc_currency = $_REQUEST['currency'];
			WC()->session->set('wc_currency', $wc_currency);
			$wc_position = $settings_mcs[$wc_currency]['nbt_'.NBT_Solutions_Currency_Switcher::$plugin_id.'_position'];

		}



		if(WC()->session->get('wc_currency')){
			$wc_currency = WC()->session->get('wc_currency');

			if(isset($settings_mcs[$wc_currency]['nbt_'.NBT_Solutions_Currency_Switcher::$plugin_id.'_position'])){
				$wc_position = $settings_mcs[$wc_currency]['nbt_'.NBT_Solutions_Currency_Switcher::$plugin_id.'_position'];
			}
			
		}
		

		?>
		<div class="nbtcs-w-wrapper">
			<span class="selected"><?php echo NBT_Solutions_Currency_Switcher::show_position($wc_currency, $wc_position, get_woocommerce_currency_symbol( $wc_currency ));?></span>
			<div class="nbtcs-select">
				<?php
					if($settings_mcs){
						foreach ($settings_mcs as $key => $value) {
							?>
							<a href="<?php echo $current_url.$touch;?>currency=<?php echo $value['nbt_currency-switcher_repeater_currency'];?>" data-value="<?php echo $value['nbt_currency-switcher_repeater_currency'];?>"><?php echo NBT_Solutions_Currency_Switcher::show_position($value['nbt_currency-switcher_repeater_currency'], $value['nbt_'.NBT_Solutions_Currency_Switcher::$plugin_id.'_position'], $value['nbt_currency-switcher_repeater_symbol']);?></a>
							<?php
						}
					} ?>		
			</select>

		</div>

		<?php
	}

	/**
	 * Enqueue scripts and stylesheets
	 */
	public function embed_style() {

		if( ! defined('PREFIX_NBT_SOL')){
			wp_enqueue_style( 'nbt-fonts', NBT_MCS_URL .'assets/css/nbt-fonts.css', array(), '20160615' );
			wp_enqueue_style( 'cs-frontend', NBT_MCS_URL .'assets/css/frontend.css', array(), '20160615' );
		}

		if( ! defined('PREFIX_NBT_SOL')){
			wp_enqueue_script( 'cs-frontend', NBT_MCS_URL . 'assets/js/frontend.js', array( 'jquery' ), time(), true );
			wp_localize_script( 'cs-frontend', 'nbtcs', array(
				'ajax_url' => admin_url( 'admin-ajax.php' )
			));
		}
	}
}
new NBT_CS_Frontend();