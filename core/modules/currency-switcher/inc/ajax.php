<?php
class NBT_MCS_Ajax{

	protected static $initialized = false;
	
    /**
     * Initialize functions.
     *
     * @return  void
     */
    public static function initialize() {
        if ( self::$initialized ) {
            return;
        }

	    self::admin_hooks();
        self::$initialized = true;
    }


    public static function admin_hooks(){
		add_action( 'wp_ajax_nopriv_load_rates', array( __CLASS__, 'nbtmcs_load_rates') );
		add_action( 'wp_ajax_load_rates', array(  __CLASS__, 'nbtmcs_load_rates') );

        add_action( 'wp_ajax_nopriv_save_mcs', array( __CLASS__, 'nbtmcs_save_mcs') );
        add_action( 'wp_ajax_save_mcs', array(  __CLASS__, 'nbtmcs_save_mcs') );

        add_action( 'wp_ajax_nopriv_nbtmcs_save_select', array( __CLASS__, 'nbtmcs_save_select') );
        add_action( 'wp_ajax_nbtmcs_save_select', array(  __CLASS__, 'nbtmcs_save_select') );
    }

    public static function nbtmcs_save_select(){
    	$settings_mcs = get_option(NBT_Solutions_Currency_Switcher::$plugin_id.'_settings');
        $settings_mcs = $settings_mcs['nbt_currency-switcher_repeater'];
    	$currency = $_REQUEST['currency'];


        $key = array_search($currency, array_column($settings_mcs, 'nbt_currency-switcher_repeater_currency'));

    	if(isset($settings_mcs[$key])){
    		WC()->session->set('wc_currency', $currency);
    		$json['complete'] = true;

    	}
    	echo wp_json_encode($json, TRUE);
    	wp_die();
    }

    public static function nbtmcs_load_rates(){
    	$currency = $_REQUEST['currency'];
        $currency_code_options = get_woocommerce_currencies();
        $from = get_woocommerce_currency();

        if(isset($currency_code_options[$currency])){
            if($rate = NBT_Solutions_Currency_Switcher::get_rate($from, $currency)){
                $json['complete'] = true;
                $json['rate'] = $rate;
            }

        }else{
            $json['alert'] = 'error';
        }

        echo json_encode($json, TRUE);
    	wp_die();
    }

    public static function nbtmcs_save_mcs(){
        $currency = $_REQUEST['currency'];
        $position = $_REQUEST['position'];
        $decimals = $_REQUEST['decimals'];
        $rates = $_REQUEST['rates'];
        $image = $_REQUEST['image'];
        $currency_code_options = get_woocommerce_currencies();

        $currency_filter = array_filter($currency);
        if(count($currency_filter) == count($currency)){
            $new = array();
            foreach ($currency as $key => $value) {
                $new[$value] = array(
                    'position' => $position[$key],
                    'decimals' => $decimals[$key],
                    'rates' => $rates[$key],
                    'image' => $image[$key],
                    'symbol' => $value
                );

            }
           update_option(NBT_Solutions_Currency_Switcher::$plugin_id.'_settings', $new);
            $json['complete'] = true;
        }

        echo json_encode($json, TRUE);
        wp_die();
    }

}