jQuery( function( $ ) {
	var $el_cs = $( '.pm_repeater');

	var cs_load = {
		/**
		 * Init jQuery.BlockUI
		 */
		block: function() {
			$el_cs.block({
				message: null,
				overlayCSS: {
					background: '#fff',
					opacity: 0.6
				}
			});
		},

		/**
		 * Remove jQuery.BlockUI
		 */
		unblock: function() {
			$el_cs.unblock();
		}
	}
	/**
	 * Variations Price Matrix actions
	 */
	var nbtmcs_admin = {

		/**
		 * Initialize variations actions
		 */
		init: function() {
			$(document).on('click', '.pm-icon.-minus', this.remove_row);
			$(document).on('click', '.pm-icon.-plus', this.add_row);
			$(document).on('click', '.nbt-load-rate', this.load_rates);
			//$(document).on('click', '.save-mcs', this.save_mcs);
			$(document).on('click', '.nbtmcs-upload-image-button', this.upload_image);
			$(document).on( 'click', '.nbtmcs-remove-image-button', this.remove_upload_image);

			$( '.mcs-select2' ).select2();
		},
		close_dialog: function(){
			$('.overlay-gallery').hide();
		},
		load_rates: function(){
			cs_load.block();
			var $this = $(this);
			var currency = $(this).closest('.pm-row').find('.nbt_currency-switcher_repeater_currency').select2('val');

			if(currency != 0){
				$.ajax({
					url: nbtmcs.ajax_url,
					data: {
						action:     'load_rates',
						currency: currency
					},
					type: 'POST',
					datatype: 'json',
					success: function( response ) {
						var rs = JSON.parse(response);
						
						if(rs.complete != undefined){
							$this.closest('.pm-input-wrap').find('.nbt_currency-switcher_rates').val(rs.rate);
						}
						cs_load.unblock();

					},
					error:function(){
						alert('There was an error when processing data, please try again !');
					}
				});
			}else{
				alert('Please select an currency!');
				cs_load.unblock();
			}

			return false;
		},
		save_mcs: function(){
			cs_load.block();
			var mcs_currency = $("select[name='mcs_currency[]']")
              .map(function(){return $(this).val();}).get();
			var mcs_position = $("select[name='mcs_position[]']")
              .map(function(){return $(this).val();}).get();
			var mcs_decimals = $("select[name='mcs_decimals[]']")
              .map(function(){return $(this).val();}).get();
			var mcs_rates = $("input[name='mcs_rates[]']")
              .map(function(){return $(this).val();}).get();   
              var mcs_img = $(".nbtmcs-term-image")
              .map(function(){return $(this).val();}).get();   

				$.ajax({
					url: nbtmcs.ajax_url,
					data: {
						action:     'save_mcs',
						currency: mcs_currency,
						position: mcs_position,
						decimals: mcs_decimals,
						rates: mcs_rates,
						image: mcs_img
					},
					type: 'POST',
					datatype: 'json',
					success: function( response ) {
						var rs = JSON.parse(response);
						

						cs_load.unblock();

					},
					error:function(){
						alert('There was an error when processing data, please try again !');
						cs_load.unblock();
					}
				});
		},
		add_row: function(){
			var $row = $('#table-mcs tbody > tr:first-child').html();
			var $tr = $( this ).closest('.pm-row');
			var $count = $('#table-mcs tbody > tr').length;
			var $option = $('#table-mcs tbody > tr:first-child').find('select.pm-attributes-field option').length - 1;
			
			if($count < $option){
				$tr.after('<tr class="pm-row" id="pm-row-' +  $count + '" data-id="' + $count + '">' + $row + '</tr>');

				$('#pm-row-' +  $count+ ' .select2-container').remove();
				$('#pm-row-' +  $count+ ' .mcs-select2').removeClass('select2-hidden-accessible');

				$('#pm-row-' +  $count+ ' .mcs-select2' ).select2();

				$( "#table-mcs tbody > tr" ).each(function(index) {
					$(this).find('.order span').text(index + 1);

				});
			}
			return false;
		},
		remove_row: function(){
			var $count = $('.pm_repeater tbody > tr').length;
			if($count > 2){
				$(this).closest('.pm-row').remove();
			}else{
				alert('Sorry, you can\'t remove this row!');
			}
			return false;
		},
		upload_image: function(e){

				e.preventDefault();
				var $button = $( this ).closest('.nbtmcs-wrap-image');
				
		

			// Create the media frame.
			var frame = wp.media.frames.downloadable_file = wp.media( {
				title   : nbtmcs.i18n.mediaTitle,
				button  : {
					text: nbtmcs.i18n.mediaButton
				},
				multiple: false
			} );

			// When an image is selected, run a callback.
			frame.on( 'select', function () {
				var attachment = frame.state().get( 'selection' ).first().toJSON();
				$button.addClass('class_name' + attachment.id);
				$button.find( 'input.nbtmcs-term-image' ).val( attachment.id );
				$button.find( '.nbtmcs-remove-image-button' ).show();
				$button.find( 'img' ).attr( 'src', attachment.sizes.thumbnail.url );
			} );

			// Finally, open the modal.
			frame.open();
		},
		remove_upload_image: function(){
			var $button = $( this );

			$button.siblings( 'input.nbtmcs-term-image' ).val( '' );
			$button.siblings( '.nbtmcs-remove-image-button' ).show();
			$button.parent().prev( '.nbtmcs-term-image-thumbnail' ).find( 'img' ).attr( 'src', nbtmcs.placeholder );

			return false;
		}
		
	}

	
	nbtmcs_admin.init();

});