jQuery( function( $ ) {

	var nbtcs_load = {
		/**
		 * Init jQuery.BlockUI
		 */
		block: function($el) {
			$('.nbt-page-loading').remove();
			$el.append('<div class="nbt-page-loading"><span class="nbt-loading-svg"></span></div>');
		},

		/**
		 * Remove jQuery.BlockUI
		 */
		unblock: function($el) {
			$('.nbt-page-loading').remove();
		}
	}

	var nbtcs_main = {

		/**
		 * Initialize variations actions
		 */
		init: function() {
        	$(document).on('click', '.nbtcs-w-wrapper .selected', this.show_language);
        	$(document).on('click', '.nbtcs-select a', this.set_language);
			$(document).mouseup(function(e) 
			{
				if($(".nbtcs-w-wrapper").length){
				    var container = $(".nbtcs-select");
				    var icon = $(".nbtcs-w-wrapper .selected");

				    if (!container.is(e.target) && container.has(e.target).length === 0 && !icon.is(e.target) && icon.has(e.target).length === 0) 
				    {
						container.hide();
						container.removeClass('open');
				    }

				    if(icon.is(e.target)){

				    }
				}

			});

			
			
		},
		show_language: function(){
			if($(this).hasClass('active')){
				$('.nbtcs-select').hide();
				$(this).removeClass('active');
			}else{
				$('.nbtcs-select').show();
				$(this).addClass('active');
			}
		},
		set_language: function(){
			nbtcs_load.block($('body'));
			if(typeof nbt_solutions !== 'undefined' && nbt_solutions.ajax_url != undefined){
				ajax_url = nbt_solutions.ajax_url;
			}else{
				ajax_url = nbtcs.ajax_url;
			}
			$.ajax({
				url: ajax_url,
				data: {
					action:     'nbtmcs_save_select',
					currency: $(this).attr('data-value'),
					href: $(this).attr('href'),
				},
				type: 'POST',
				datatype: 'json',
				success: function( response ) {
					
					var rs = JSON.parse(response);
					if(rs.complete != undefined){
						location.reload();
					}
					$('.nbtcs-select').hide();


				},
				error:function(){
					alert('There was an error when processing data, please try again !');
					nbtcs_load.unblock($('body'));
				}
			});
			return false;
		}
	}


	nbtcs_main.init();
	
});