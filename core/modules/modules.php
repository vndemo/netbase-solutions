<?php
/**
 * @version    1.0
 * @package    Package Name
 * @author     Your Team <support@yourdomain.com>
 * @copyright  Copyright (C) 2014 yourdomain.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 */

/**
 * Plug additional sidebars into WordPress.
 *
 * @package  Package Name
 * @since    1.0
 */
class NBT_Solutions_Modules {
	/**
	 * Variable to hold the initialization state.
	 *
	 * @var  boolean
	 */
	protected static $initialized = false;

	public static $settings = array();
	public static $modules_settings = array();
	/**
	 * Initialize functions.
	 *
	 * @return  void
	 */
	public static function initialize() {
		// Do nothing if pluggable functions already initialized.
		if ( self::$initialized ) {
			return;
		}

		// Register actions to do something.
		add_action( 'init'   , array( __CLASS__, 'load_modules'    ) );
		// State that initialization completed.
		self::$initialized = true;
	}

	/**
	 * Method Featured.
	 *
	 * @return  array
	 */
	public static function register_modules() {
		$modules = array(
			'ajax-cart' => array(
				'label' => 'Ajax Cart',
				'class' => 'Ajax_Cart',
				'description' => __('Change the default behavior of Add To Cart buttons of WooCommerce and improve user experience when purchasing products.', 'nbt-solution')
			),
			'ajax-search' => array(
				'label' => 'Ajax Search',
				'class' => 'Ajax_Search',
				'description' => __('Improve user experience by make customers find his/her products more easily.', 'nbt-solution')
			),
			'currency-switcher' => array(
				'label' => 'Multi-currency',
				'class' => 'Currency_Switcher',
				'description' => __('Make your site use multiple currencies at the same time.', 'nbt-solution')
			),
			'color-swatches' => array(
				'label' => 'Color Swatches',
				'class' => 'Color_Swatches',
				'description' => __('Change default select box of attributes to something more intuitive: Radio images, Radio buttons, Color, etc.', 'nbt-solution')
			),
			'brands' => array(
				'label' => 'Shop by Brands',
				'class' => 'Brands',
				'description' => __('Assign products to a brand and let customers filter products by those brands.', 'nbt-solution')
			),
			'order-upload' => array(
				'label' => 'Order Upload',
				'class' => 'Order_Upload',
				'description' => __('Function for your customers now can upload file(s) and attach it to the order.', 'nbt-solution')
			),
			'product-notification' => array(
				'label' => 'Product Notification',
				'class' => 'Product_Notification',
				'description' => __('Customers can register so they will be notified if your products are in stock or not', 'nbt-solution')
			),
			'one-step-checkout' => array(
				'label' => 'One Step Checkout',
				'class' => 'One_Step_Checkout',
				'description' => __('Reduce the checkout process by offering the entire purchase process on a single page', 'nbt-solution')
			),
			'faqs' => array(
				'label' => 'FAQs',
				'class' => 'Faqs',
				'description' => __('Allow customers to ask question or seek answer about products before purchase', 'nbt-solution')
			),
			'pdf-creator' => array(
				'label' => 'PDF Creator',
				'class' => 'Pdf_Creator',
				'description' => __('Allow customers print their orders in beatiful PDF format', 'nbt-solution')
			),
			'price-matrix' => array(
				'label' => 'Price Matrix',
				'class' => 'Price_Matrix',
				'description' => __('Allow customers easy enter price for variation product.', 'nbt-solution')
			),
			'frequently-bought-together' => array(
				'label' => 'Frequently Bought Together',
				'class' => 'Frequently_Bought_Together',
				'description' => __('Allow customers easy enter price for variation product.', 'nbt-solution')
			),
			'social-login' => array(
				'label' => 'Social Login',
				'class' => 'Social_Login',
				'description' => __('Allow customers easy enter price for variation product.', 'nbt-solution')
			),
			'live-chat' => array(
				'label' => 'Live Chat',
				'class' => 'Live_Chat',
				'description' => __('Embeds Tawk.to live chat widget to your site.', 'nbt-solution')
			),
			'metabox' => array(
				'label' => 'Metabox',
				'class' => 'Metabox',
				'hide' => true,
				'description' => __('', 'nbt-solution')
			),
			'smtp' => array(
				'label' => 'SMTP',
				'class' => 'SMTP',
				'hide' => true,
				'description' => __('', 'nbt-solution'),
			),
			'gallery-slider' => array(
				'label' => 'Gallery Slider',
				'class' => 'Gallery_Slider',
				'required' => true,
				'description' => __('', 'nbt-solution')
			)
		);

		return $modules;
	}

	public static function load_modules(){
		$register_modules = self::register_modules();
		$settings_modules = get_option('solutions_core_settings' );
		if($settings_modules){
			foreach ($settings_modules as $key => $modules) {

				if(isset($register_modules[$modules]['class'])){
					$_modules = $register_modules[$modules]['class'];
					if (class_exists('NBT_Solutions_'.$_modules)) {
						call_user_func('NBT_Solutions_'.$_modules .'::initialize');

						/* Check settings modules */
						if( isset($register_modules[$modules]['class']) && file_exists(PREFIX_NBT_SOL_PATH.'core/modules/'.$modules.'/inc/settings.php')){
							self::$modules_settings[$modules] = array(
								'class' => $_modules,
								'label' => $register_modules[$modules]['label']
							);
						}
					 
					}
				}
			}

			if(!empty(self::$modules_settings) && count(self::$modules_settings) > 0){
				add_action('admin_menu', array(__CLASS__, 'add_menu_settings'));
				foreach (self::$modules_settings as $modules => $_modules) {
					include(PREFIX_NBT_SOL_PATH.'core/modules/'.$modules.'/inc/settings.php');
					self::$settings[$modules]['settings'] = call_user_func('NBT_'.$_modules['class'].'_Settings::get_settings');
					if(self::$settings[$modules]){
						$value_modules = get_option($modules.'_settings');

						self::$settings[$modules]['module_name'] = $_modules['label'];
						self::$settings[$modules]['slug'] = $modules;
						foreach (self::$settings[$modules]['settings'] as $set => $val) {

						    if(isset($val['id']) && isset($value_modules[$val['id']])){
                                self::$settings[$modules]['settings'][$set]['value'] = $value_modules[$val['id']];
                            }

                            if(isset($val['id']) && $val['type'] == 'repeater'){
                                self::$settings[$modules]['settings'][$set]['value'] = $value_modules[$val['id']];
                            }

						}
					}
				}
			}
		}
	}

	public static function add_menu_settings(){
		add_submenu_page('solutions', 'NBT Solutions Settings', 'Settings', 'manage_options', 'solutions-settings', array( __CLASS__, 'page_solutions_settings')); 
	}



	public static function page_solutions_settings(){
		if(!empty(self::$settings)){
			include(PREFIX_NBT_SOL_PATH.'core/modules/settings.php');			
		}
	}
}




