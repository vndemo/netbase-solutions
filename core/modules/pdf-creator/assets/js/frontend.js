
jQuery(document).ready(function($){
	var x = false;

	var nbtpdf_js = {
		init: function(){

			$(document).on('click', '.btn-pdf-preview', this.save_id);

		},
		save_id: function(){
			if($(this).hasClass('active')){
				$(this).next().slideUp();
				$(this).removeClass('active');
			}else{
				$(this).next().slideDown();
				$(this).addClass('active');
			}
		}
	}
	nbtpdf_js.init();
});