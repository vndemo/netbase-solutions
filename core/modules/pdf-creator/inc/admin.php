<?php
class NBT_PDF_Admin {
	/**
	 * Class constructor.
	 */
	public function __construct() {
		add_action( 'admin_enqueue_scripts', array($this, 'nbt_pdf_scripts_method') );
		add_action( 'woocommerce_admin_order_actions_end', array( $this, 'add_listing_actions' ) );

	}

	public function nbt_pdf_scripts_method($hooks){
		wp_enqueue_style( 'pdf_admin', NBT_PDF_URL . 'assets/css/admin.css'  );
	}

	public function add_listing_actions( $order ){
		// do not show buttons for trashed orders
		if ( $order->get_status() == 'trash' ) {
			return;
		}


		$data = array(
				'url'		=> wp_nonce_url( admin_url( "admin-ajax.php?action=nbtpdf_download&order_id=" . $order->get_id() ), 'generate_wpo_wcpdf' ),
				'img'		=> NBT_PDF_URL . "assets/img/invoice.png",
				'alt'		=> __('PDF Invoice', 'nbt-solutions'),
		);

		?>
				<a href="<?php echo $data['url']; ?>" class="button tips nbt_pdf-creator" target="_blank" alt="<?php echo $data['alt']; ?>" data-tip="<?php echo $data['alt']; ?>">
					<img src="<?php echo $data['img']; ?>" alt="<?php echo $data['alt']; ?>" width="16">
				</a>
		<?php

	}
}
new NBT_PDF_Admin();