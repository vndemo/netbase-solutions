<?php
class NBT_PDF_Frontend {
	/**
	 * Class constructor.
	 */
	public function __construct() {
		add_action( 'woocommerce_thankyou', array($this, 'nbt_woocommerce_payment_complete'), 10, 1 );

	}

	public function nbt_woocommerce_payment_complete($order_id){
		$key = get_post_meta($order_id, '_order_key', true);
		echo sprintf('<a href="'.get_permalink(NBT_Solutions_Pdf_Creator::page_template()).'?key='.$key.'" class="btn btn-link btn-pdf-preview" target="_blank">%1$s</a>', __('View or Print PDF Invoice', 'nbt-solutions'));
	}

}
new NBT_PDF_Frontend();