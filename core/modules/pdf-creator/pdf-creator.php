<?php
/**
 * @version    1.0
 * @package    Package Name
 * @author     Your Team <support@yourdomain.com>
 * @copyright  Copyright (C) 2014 yourdomain.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 */

/**
 * Plug additional sidebars into WordPress.
 *
 * @package  Package Name
 * @since    1.0
 */
define('NBT_PDF_PATH', plugin_dir_path( __FILE__ ));
define('NBT_PDF_URL', plugin_dir_url( __FILE__ ));

class NBT_Solutions_Pdf_Creator {
    /**
     * Variable to hold the initialization state.
     *
     * @var  boolean
     */
    protected static $initialized = false;

    public static $types = array();
    
    /**
     * Initialize functions.
     *
     * @return  void
     */
    public static function initialize() {
        // Do nothing if pluggable functions already initialized.
        if ( self::$initialized ) {
            return;
        }

        if ( ! function_exists( 'WC' ) ) {
            add_action( 'admin_notices', array( __CLASS__, 'install_woocommerce_admin_notice') );
        }else{

            add_action( 'init', array( __CLASS__, 'load_textdomain' ) );

            
            require_once 'inc/admin.php';
            require_once 'inc/register_template.php';
            if( ! defined('PREFIX_NBT_SOL')){
                include(NBT_PDF_PATH . 'inc/settings.php');
            }
            if(!is_admin()){
                require_once 'inc/frontend.php';
            }

            if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
                require_once 'inc/ajax.php';
                NBT_Pdf_Creator_Ajax::initialize();
            }

            if( !get_option('_create_page_pdf') ){
                $create = array(
                    'post_title' => 'PDF Preview',
                    'post_type' => 'page',
                    'post_status' => 'publish',
                    'post_content' => '',
                    'post_slug' => 'pdf-preview'
                );
                $post_id = wp_insert_post($create);

                update_post_meta($post_id, '_wp_page_template', 'pdf_preview.php');
                update_option('_create_page_pdf', true);
            }

        }
        // State that initialization completed.
        self::$initialized = true;
    }

    public static function page_template(){
        global $wpdb;
        return $wpdb->get_var( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '_wp_page_template' AND meta_value = 'pdf_preview.php'" );
    }

    public static function pdf_maker( $html, $settings = array() ) {
        if ( ! class_exists( 'NBT_PDF_Maker' ) ) {
            include_once( NBT_PDF_PATH . 'inc/pdf-maker.php' );
        }
        $class = apply_filters( 'nbt_pdf_maker', 'NBT_PDF_Maker' );
        return new $class( $html, $settings );
    }

    public static function get_temp(){
        $upload_dir = wp_upload_dir();
        return trailingslashit( $upload_dir['basedir'] );
    }

    public static function wpdf_headers( $filename, $mode = 'inline', $pdf = null ) {
        switch ($mode) {
            case 'download':
                header('Content-Description: File Transfer');
                header('Content-Type: application/pdf');
                header('Content-Disposition: attachment; filename="'.$filename.'"'); 
                header('Content-Transfer-Encoding: binary');
                header('Connection: Keep-Alive');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                break;
            case 'inline':
            default:
                header('Content-type: application/pdf');
                header('Content-Disposition: inline; filename="'.$filename.'"');
                break;
        }
    }

    public static function convert_price($price, $_preview){
        global $_product;
        if(class_exists('NBT_Solutions_Currency_Switcher')){
            $rate = 1;
            $pos = '%1$s%2$s';
            $symbol = get_woocommerce_currency_symbol(get_option('woocommerce_currency'));
            $decimals = get_option('woocommerce_price_num_decimals' );

            if(method_exists(WC()->session,'get')){
                $ss_wc_currency = WC()->session->get('wc_currency');
                if($ss_wc_currency){
                    $settings_mcs = get_option('settings_mcs');
                    $currency = $settings_mcs[$ss_wc_currency];
                    $rate = $currency['rates'];
                    $position = $currency['position'];
                    $decimals = $currency['decimals'];
                    $symbol = $currency['symbol'];
                    

                    switch ($position) {
                        case 'left' :
                        $pos = '%1$s%2$s';
                        break;
                        case 'right' :
                        $pos = '%2$s%1$s';
                        break;
                        case 'left_space' :
                        $pos = '%1$s&nbsp;%2$s';
                        break;
                        case 'right_space' :
                        $pos = '%2$s&nbsp;%1$s';
                        break;
                    }
                    $final_price = $price * $rate;
                    $final_price = number_format( $final_price, $decimals, wc_get_price_decimal_separator(), wc_get_price_thousand_separator() );
                    $pos = str_replace('%2$s', $final_price, $pos);
                    echo str_replace('%1$s', $symbol, $pos);


                }else{

                }
            }


                            // return ac_get_price($_product, array(
                            //     'rate' => $ajaxcart_rate,
                            //     'position' => $ajaxcart_pos,
                            //     'symbol' => $ajaxcart_symbol,
                            //     'decimals' => $ajaxcart_decimals
                            // ));

        }

        
    }
}

