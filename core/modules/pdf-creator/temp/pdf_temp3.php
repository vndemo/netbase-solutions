<?php
$primary_color = '#363a45';
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		
		<?php if(isset($_preview)){?>
		<link rel='stylesheet' id='dashicons-css'  href='<?php echo NBT_PDF_URL;?>temp/css/fonts.css' type='text/css' media='all' />
		<link rel='stylesheet' id='dashicons-css'  href='<?php echo $font_css;?>' type='text/css' media='all' />
		<script type='text/javascript' src='<?php echo home_url();?>/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
		<script type='text/javascript' src='<?php echo home_url();?>/wp-includes/js/jquery/jquery-migrate.js?ver=1.4.1' defer onload=''></script>
		<script type='text/javascript' src='<?php echo NBT_PDF_URL;?>assets/js/jquery.blockUI.js' defer onload=''></script>
		
		<script type="text/javascript">
			jQuery(document).ready(function($){
				$('.btn-print-pdf').click(function(){
					$('.preview').block({
						message: null,
						overlayCSS: {
							background: '#fff',
							opacity: 0.6
						}
					});

					$.ajax({
						url: '<?php echo admin_url('admin-ajax.php');?>',
						data: {
							action:     'nbtpdf_download',
							order_id: <?php echo $order->get_id();?>
						},
						type: 'POST',
						datatype: 'json',
						success: function( response ) {
							$('.preview').unblock();
						},
						error:function(){
							alert('There was an error when processing data, please try again !');
							$('.preview').unblock();
						}
					});

					return false;
				});
			});
		</script>
		<?php }?>
		<link rel='stylesheet' id='dashicons-css'  href='<?php echo NBT_PDF_URL;?>temp/css/pdf_temp3.css' type='text/css' media='all' />

		<style type="text/css">
			body{
				color: <?php echo $text_color;?>
			}
			.table thead{
				background: <?php echo $primary_color;?>
			}
			hr{
				border-color: <?php echo $primary_color;?>;
			}
			.col-left-to h2, .col-right-to-wrap .label, .col-left h1{
				color: <?php echo $primary_color;?>;
			}
		</style>
	</head>
<body class="preview">
	<div class="clearfix">
		<div class="col-left">
				<img class="logo" src="<?php echo $logo;?>" />
				<p><?php echo $address;?></p>
		</div>

		<div class="col-right">
			<h1><?php _e('Invoice', 'nbt-pdf-creator');?></h1>
		</div>
	</div>

	<div class="height30"></div>

	<div class="clearfix">
		<div class="top-header">
			<div><span><?php _e('Invoice', 'nbt-pdf-creator');?> #<?php echo $order->get_order_number();?></span></div>
			<div class="separator"><span>|</span></div>
			<div><span><?php _e('Date', 'nbt-pdf-creator');?> <?php echo wc_format_datetime( $order->get_date_created() ); ?></span></div>
		</div>
		<div class="clear"></div>
	</div>

	<table class="body-detail">
		<tr>
			<td class="body-heading body-first"><h2>Bill to</h2></td>
			<td>
				<div class="bodydetail">
					<h3>Jonathan Nguyen</h3>
					<p><?php echo $address;?></p>
				</div>
			</td>
			<td class="space">&nbsp;</td>
			<td class="body-heading"><h2>Ship to</h2></td>
			<td class="body-last">
				<div class="bodydetail">
					<h3>Jonathan Nguyen</h3>
					<p><?php echo $address;?></p>
				</div>
			</td>
		</tr>
	</table>


	<div class="clearfix group-table">
		<table class="table table-bordered">
			<thead>
			  <tr>
			    <th style="width: 50%" class="text-left"><?php _e('Product Name', 'nbt-solutions');?></th>
			    <th style="width: 10%" class="qty"><?php _e('Qty', 'nbt-solutions');?></th>
			    <th style="width: 20%"><?php _e('Price', 'nbt-solutions');?></th>
			    <th style="width: 20%"><?php _e('Total', 'nbt-solutions');?></th>
			  </tr>
			</thead>
			<tbody>
				<?php foreach ( $order->get_items() as $item ) {
					$product_id = $item['product_id'];
					$_product = wc_get_product( $product_id );
					?>
				<tr>
					<td class="text-left first-child"><?php echo $item->get_name();?></td>
					<td class="text-center"><?php echo $item->get_quantity();?></td>
					<td class="text-right padding_r10"><?php echo NBT_Solutions_Pdf_Creator::convert_price($_product->get_price(), $_preview);?></td>
					<td class="text-right padding_r10 last-child"><?php echo NBT_Solutions_Pdf_Creator::convert_price($item->get_total(), $_preview);?></td>
				</tr>
				<?php }?>
			</tbody>
		</table>


		<table class="subtotal-table">
			<?php if( $subtotal = (float)$order->get_subtotal() ){?>
				<tr class="cart_subtotal">
					<td class="subtotal-empty">&nbsp;</td>
					<td class="label text-right"><?php esc_html_e( 'Subtotal', 'woocommerce' ); ?></td>
					<td class="span text-right"><?php echo NBT_Solutions_Pdf_Creator::convert_price($subtotal, $_preview); ?></td>
				</tr>
			<?php }?>

				<tr class="order_total">
					<td class="subtotal-empty">&nbsp;</td>
					<td class="label text-right"><?php esc_html_e( 'Tax Rate', 'woocommerce' ); ?></td>
					<td class="span text-right"><?php echo number_format((float)get_post_meta($order_id, '_order_tax', true), 2); ?>%</td>
				</tr>
		</table>

		<?php if( $gettotals = (float)$order->get_total() ){?>
		<div class="total-full clearfix">
			<div class="total-col1">&nbsp;</div>
			<div class="total-label"><label><?php esc_html_e( 'Total', 'woocommerce' ); ?></label></div>
			<div class="total-price"><?php echo NBT_Solutions_Pdf_Creator::convert_price($gettotals, $_preview); ?></div>
		</div>
		<?php }?>
	</div>
	
	<p class="note-temp"><?php echo sprintf(__('Thank you for your business<br />payment is due max 7 days after invoice without deduction.', 'nbt-pdf-creator'));?></p>


		<?php
		if(isset($_preview)){?>
			<button type="button" class="btn btn-link btn-print-pdf"><i class="nbt-icon-file-pdf"></i> Print PDF</button>
		<?php }?>
</body>
</html>
