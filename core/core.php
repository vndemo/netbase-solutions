<?php
/**
 * 1.3.3    1.0
 * @package    Package Name
 * @author     Your Team Name <support@yourdomain.com>
 * @copyright  Copyright (C) 2014 yourdomain.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://www.yourdomain.com
 */

// Prevent direct access to this file
defined( 'ABSPATH' ) || die( 'Direct access to this file is not allowed.' );

/**
 * Core class.
 *
 * @package  Package Name
 * @since    1.0
 */
define('NBT_CORE_PATH', plugin_dir_path( __FILE__ ));
class PREFIX_CORE{
	/**
	 * Define theme version.
	 *
	 * @var  string
	 */
	const VERSION = '1.0.0';

	/**
	 * Define valid class prefix for autoloading.
	 *
	 * @var  string
	 */
	protected static $prefix = 'NBT_Solutions_';

	/**
	 * Initialize Package Name.
	 *
	 * @return  void
	 */
	public static function initialize() {
		// Register class autoloader.
		spl_autoload_register( array( __CLASS__, 'autoload' ) );
		
//		print_r('Hello This Core');

		// Include function plugins if not include.
		self::include_function_plugins();

		// Register necessary actions.
		add_action( 'admin_menu', array( __CLASS__, 'admin_menu' ) );

		// Add custom css to admin
		add_action( 'admin_head', array( __CLASS__, 'admin_custom_css' ) );		
		add_action( 'init', array( __CLASS__, 'stop_heartbeat'), 1 );

		// add_action( 'admin_enqueue_scripts', array(__CLASS__, 'admin_enqueue' ));

		// Plug into WordPress and supported 3rd-party plugins.
		NBT_Solutions_Pluggable::initialize();
		NBT_Solutions_Update::initialize();

		
	}

	/**
	 * Method to autoload class declaration file.
	 *
	 * @param   string  $class_name  Name of class to load declaration file for.
	 *
	 * @return  mixed
	 */
	public static function autoload( $class_name ) {
		// Verify class prefix.
		if ( 0 !== strpos( $class_name, self::$prefix ) ) {
			return false;
		}

		// Generate file path from class name.
        $base = PREFIX_NBT_SOL_PATH . '/core/modules/';
		$path = strtolower( str_replace( '_', '/', substr( $class_name, strlen( self::$prefix ) ) ) );

		// Check if class file exists.
		$standard    = $path . '.php';
		$alternative = $path . '/' . current( array_slice( explode( '/', str_replace( '\\', '/', $path ) ), -1 ) ) . '.php';

		while ( true ) {
			// Check if file exists in standard path.
			if ( @is_file( $base . $standard ) ) {
				$exists = $standard;

				break;
			}

			// Check if file exists in alternative path.
			if ( @is_file( $base . $alternative ) ) {
				$exists = $alternative;

				break;
			}

			// If there is no more alternative file, quit the loop.
			if ( false === strrpos( $standard, '/' ) || 0 === strrpos( $standard, '/' ) ) {
				break;
			}

			// Generate more alternative files.
			$standard    = preg_replace( '#/([^/]+)$#', '-\\1', $standard );
			$alternative = implode( '/', array_slice( explode( '/', str_replace( '\\', '/', $standard ) ), 0, -1 ) ) . '/' . substr( current( array_slice( explode( '/', str_replace( '\\', '/', $standard ) ), -1 ) ), 0, -4 ) . '/' . current( array_slice( explode( '/', str_replace( '\\', '/', $standard ) ), -1 ) );
		}

		// Include class declaration file if exists.
		if ( isset( $exists ) ) {
			return include_once $base . $exists;
		}

		return false;
	}

	/**
	 * Include function plugins if not include.
	 *
	 * @return  void
	 *
	 * @since  1.1.8
	 *
	 */
	public static function include_function_plugins() {
        if ( ! function_exists( 'is_plugin_active' ) ) {
            require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        }

        if ( ! function_exists( 'wp_get_current_user' ) ) {
            require_once( ABSPATH . 'wp-includes/pluggable.php' );
        }
	}

	/**
	 * Creates a new top level menu section.
	 *
	 * @return  void
	 */
	public static function admin_menu() {
		
	}

	/**
	 * Add some custom css to control notice of 3rd-plugin
	 *
	 * @since  1.1.8
	 */
	public static function admin_custom_css() {
		echo '
			<style>
				.vc_license-activation-notice.updated,
				.rs-update-notice-wrap.updated,
				.installer-q-icon {
					display: none;
				}
			</style>
		';
	}

   public static function write_log ( $log )  {
      if ( is_array( $log ) || is_object( $log ) ) {
         error_log( print_r( $log, true ) );
      } else {
         error_log( $log );
      }
   }



	public static function stop_heartbeat() {
		wp_deregister_script('heartbeat');
	}
	
}

// Initialize PREFIX.
PREFIX_CORE::initialize();


if(!function_exists('log_it')){
	function log_it( $message ) {
	  if( WP_DEBUG === true ){
		if( is_array( $message ) || is_object( $message ) ){
		  error_log( print_r( $message, true ) );
		} else {
		  error_log( $message );
		}
	  }
	}
   }