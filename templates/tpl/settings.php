<div class="wrap" id="panels-settings-page">
	<h1><?php _e('NBT Solutions Core', 'nbt-solution-core') ?></h1>
	<?php if( self::$settings_saved ) : ?>
		<div id="setting-error-settings_updated" class="updated settings-error">
			<p><strong><?php _e('Settings Saved', 'siteorigin-panels') ?></strong></p>
		</div>
	<?php endif; ?>

    <form action="<?php echo admin_url('admin.php?page=solutions') ?>" method="post" >
		<ul>
			<?php foreach ($register_modules as $key_modules => $modules) {
				$values = '';
				if(is_array($settings_modules) && in_array($key_modules, $settings_modules)){
					$values = $key_modules;
				}
				?>
				<li><label for="<?php echo $key_modules;?>"><input type="checkbox" name="nbt_solutions_func[]" id="<?php echo $key_modules;?>" value="<?php echo $key_modules;?>" <?php checked( ! empty( $values ) ) ?> /> <?php echo $modules['label'];?></label></li>
			<?php }?>
		</ul>
		<div class="submit">
			<?php wp_nonce_field( 'solution-settings' ) ?>
			<input type="submit" value="<?php _e('Save Changes', 'nbt-solution-core') ?>" class="button-primary" />
		</div>
	</form>

</div>