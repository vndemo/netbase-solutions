<?php
/*
Plugin Name: Netbase Solutions
Plugin URI: http://cmsmart.net
Description: Plugin that contain a lot of feature in Printshop Solution Package.
Version: 1.0.15
Author: Netbase-Team
Author URI: http://netbaseteam.com
Text Domain: nbt-solutions
License: GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
*/


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) )
    exit;

// Define plugin textdomain & theme templates folder for plugin.
define( 'PREFIX_NBT_SOL', 'nbt-solutions' );

// Define path to plugin directory.
define( 'PREFIX_NBT_SOL_PATH', plugin_dir_path( __FILE__ ) );

// Define URL to plugin directory.
define( 'PREFIX_NBT_SOL_URL', plugin_dir_url( __FILE__ ) );

// Define plugin base file.
define( 'PREFIX_NBT_SOL_BASENAME', plugin_basename( __FILE__ ) );

define( 'PREFIX_NBT_SOL_DEV', true );



add_filter('pre_site_transient_update_core', 'remove_core_updates');
add_filter('pre_site_transient_update_plugins', 'remove_core_updates');
add_filter('pre_site_transient_update_themes', 'remove_core_updates');

function remove_core_updates(){
    global $wp_version;
    return(object) array('last_checked'=> time(),'version_checked'=> $wp_version);
}

// Load the core class.
require_once PREFIX_NBT_SOL_PATH . 'core/core.php';

require_once PREFIX_NBT_SOL_PATH . 'api/api.php';
require_once PREFIX_NBT_SOL_PATH . 'dashboard.php';

